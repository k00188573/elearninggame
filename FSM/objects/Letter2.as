﻿package objects {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import states.Play2;
	
	
	
	public class Letter2 extends MovieClip {
		
		public var myLetter:String;
		
		private var playState:Play2;
		
		public function Letter(letterchar:String,playState:Play2) {
			// constructor code
            this.playState = playState;
			
			this.char.text = letterchar;
			this.myLetter = letterchar;
			
			buttonMode = true;
			addEventListener(MouseEvent.MOUSE_DOWN , pickUp)
			addEventListener(MouseEvent.MOUSE_UP , drop)

		}
		
		public function pickUp(event:MouseEvent):void
		{
			parent.addChild(this);	
			startDrag();
		}
		
		public function drop(event:MouseEvent):void
		{
			stopDrag();
			if (myLetter == "B") {
			if ((this.x >500 && this.x< 720) && (this.y > 0 && this.y< 200) ) {
				this.x = 595;
				this.y = 20;
				this.playState.letter1 = true;
		    }
		}
			if (myLetter == "A") {
			if ((this.x >800 && this.x< 1020) && (this.y > 0 && this.y< 200) ) {
				this.x = 895;
				this.y = 20;
				this.playState.letter2 = true;
		    }
		}
			if (myLetter == "L") {
			if ((this.x >1100 && this.x< 1320) && (this.y > 0 && this.y< 200) ) {
				this.x = 1195;
				this.y = 20;
				this.playState.letter3 = true;
		    }
		}
			if (myLetter == "L") {
			if ((this.x >1100 && this.x< 1320) && (this.y > 0 && this.y< 200) ) {
				this.x = 1195;
				this.y = 20;
				this.playState.letter3 = true;
		    }
		}
			

			}
	}
	
}
