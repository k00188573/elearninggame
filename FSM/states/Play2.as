﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import objects.Background;	
	import objects.Title;
	import objects.Letter2;
	import interfaces.IState;
	
	public class Play2 extends Sprite implements IState
	{
		public var game:Game;
		private var background:gameBackground;
		public var letter1:Boolean = false;
		public var letter2:Boolean = false;
		public var letter3:Boolean = false;
		public var letter4:Boolean = false;
		
		
		public function Play2(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new gameBackground();
			addChild(background);
			
			var ball = new BallImage();
			addChild(ball)
			ball.x = 700;
			ball.y = 400;	
			
			var box1 = new box();
			addChild(box1)
			box1.x =360;  
			box1.y =10;
			
			var box2 = new box();
			addChild(box2)
			box2.x =660;  
			box2.y =10;
			
			var box3 = new box();
			addChild(box3)
			box3.x =960;  
			box3.y =10;
			
			var box4 = new box();
			addChild(box4)
			box4.x =1260;  
			box4.y =10;
			
			
			
			var let1 = new Letter2("B",this);
			addChild(let1)
			let1.x = getRandomX();  
			let1.y = Math.ceil(150 + Math.random()* 650);
			
			var let2 = new Letter2("A",this);
			addChild(let2)
			let2.x = getRandomX();  
			let2.y = Math.ceil(150 + Math.random()* 650);
			
			var let3 = new Letter2("L",this);
			addChild(let3)
			let3.x = getRandomX();  
			let3.y = Math.ceil(150 + Math.random()* 650);
			
			var let4 = new Letter2("L",this);
			addChild(let3)
			let4.x = getRandomX();  
			let4.y = Math.ceil(150 + Math.random()* 650);
			
						
			var newExitButton:exitButton = new exitButton(); 
			addChild(newExitButton); 
			newExitButton.x =1183;  
			newExitButton.y =960;
			newExitButton.addEventListener(MouseEvent.CLICK, onGameOver); 
			
			var newMenuButton:menuButton = new menuButton(); 
			addChild(newMenuButton); 
			newMenuButton.x =0;  
			newMenuButton.y =960;
			newMenuButton.addEventListener(MouseEvent.CLICK, onMenu); 

			addEventListener(Event.ENTER_FRAME, chechCorrect);
		}
		
		private function randomNumber():int
		{
			var x = Math.ceil(Math.random()* 1920);
			if(x > 560 && x < 1360){
				x = randomNumber();
			}
			return x;
		}
		
			
		private function getRandomX():int
		{
			var keepGoing:Boolean = false;
			
			do {
			var x = Math.ceil(Math.random()* 1750);
			
			
			if(x > 500 && x < 1360){
				keepGoing = true;
			} else {
				keepGoing = false;
			}
			
		} while (keepGoing)
				
			return x;
		}
		
		private function chechCorrect(event:Event):void
		{
			if(letter1 && letter2 && letter3){
				letter1 = false;
				letter2 = false;
				letter3 = false;
				game.changeState(Game.GAME_OVER_STATE);
			}
		}
		
		
		private function onGameOver(event:Event):void
		{
			game.changeState(Game.GAME_OVER_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			//background.update();
			
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}