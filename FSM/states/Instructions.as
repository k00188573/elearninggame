﻿package states
{
	import Game;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import objects.Background;
	import objects.Title;

	import interfaces.IState;
	
	public class Instructions extends Sprite implements IState
	{
		public var game:Game;
		private var background:InstructionsBackground;
		
		
		public function Instructions(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new InstructionsBackground();
			addChild(background);
			
			
			var newPlayButton:PlayButton_ai = new PlayButton_ai(); 
			addChild(newPlayButton); 
			newPlayButton.x =1186;  
			newPlayButton.y =960;
			newPlayButton.addEventListener(MouseEvent.CLICK, onPlay); 
			
			var newMenuButton:menuButton = new menuButton(); 
			addChild(newMenuButton); 
			newMenuButton.x =0;  
			newMenuButton.y =960;
			newMenuButton.addEventListener(MouseEvent.CLICK, onMenu);  
		}

		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void
		{
			
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}