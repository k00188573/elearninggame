﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	
	public class GameOver extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var background2:gameOverScreen2;
	
		
		public function GameOver(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background2 = new gameOverScreen2();
			addChild(background2);	
			
			var newPlayButton:PlayButton_ai = new PlayButton_ai(); 
			addChild(newPlayButton); 
			newPlayButton.x =1183;  
			newPlayButton.y =960;
			newPlayButton.addEventListener(MouseEvent.CLICK, onPlay); 
			
			var newMenuButton:menuButton = new menuButton(); 
			addChild(newMenuButton); 
			newMenuButton.x =0;  
			newMenuButton.y =960;
			newMenuButton.addEventListener(MouseEvent.CLICK, onMenu); 
		}

		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
			
		
		private function onAgain(event:Event):void
		{
		
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void
		{
			
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}