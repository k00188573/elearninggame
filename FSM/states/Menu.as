﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	
	
	public class Menu extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
	    private var title:Title;
		
		
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			trace();
			
			background = new Background();
			addChild(background);
			
			/*var title = new Title();
			title.setTitle("Main Menu");
			title.x= 200;
			title.y=15;
			
			addChild(title);*/ 
			 
			
			var newInstructionButton:InstructionButton = new InstructionButton(); 
			addChild(newInstructionButton); 
			newInstructionButton.x =1100;  
			newInstructionButton.y =600;
			newInstructionButton.addEventListener(MouseEvent.CLICK, onInstructions);
			
			
			var newPlayButton:PlayButton_ai = new PlayButton_ai(); 
			addChild(newPlayButton); 
			newPlayButton.x =1100;  
			newPlayButton.y =400;
			newPlayButton.addEventListener(MouseEvent.CLICK, onPlay);
			
		}
		
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onInstructions(event:Event):void
		{
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		
		public function update():void
		{
			//background.update();
		}
		
		public function destroy():void
		{
			
			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}